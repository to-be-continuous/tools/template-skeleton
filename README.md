# GitLab CI template Skeleton

This is a skeleton project for starting a new _to be continuous_ template.

You shall fork it when you want to start developing a new template.

Based on the kind of template (build, analyse, hosting, acceptance, ...), you should start working from one of the available `initial-xxx` branches, that each implement basic stuff.

